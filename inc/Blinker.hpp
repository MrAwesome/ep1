#ifndef BLINKER_HPP
#define BLINKER_HPP
#include "Formas.hpp"

class Blinker : public Formas{
public:
 static const char altura_blinker = 3;
 static const char largura_blinker = 1;
 Blinker(char coordenada_x, char coordenada_y);
 ~Blinker();
};

#endif
