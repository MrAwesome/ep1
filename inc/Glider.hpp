#ifndef GLIDER_HPP
#define GLIDER_HPP
#include "Formas.hpp"

class Glider : public Formas{
public:
 static const char tamanho_glider = 3;
 Glider(char coordenada_x, char coordenada_y);
 ~Glider();
};

#endif
