#ifndef BLOCK_HPP
#define BLOCK_HPP
#include "Formas.hpp"

class Block : public Formas{
public:
  static const char tamanho_block = 4;
  Block (char coordenada_x, char coordenada_y);
  ~Block ();

};

#endif
