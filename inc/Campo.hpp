#ifndef CAMPO_HPP
#define CAMPO_HPP
#include "Formas.hpp"
#include "Glider.hpp"
#include "Blinker.hpp"
#include "Block.hpp"
#include <iostream>

using namespace std;

class Campo{
public:
 Campo(Formas f);
 void imprime_campo();
 void update();
 char getEstado(char estado, char coordenada_x, char coordenada_y, bool alternar);
 void iterar(unsigned int iteracoes);
 static const char altura = 100;
 static const char largura = 100;
private:
 char matriz[altura][largura];
 char nova_matriz[altura][largura];
 bool alternar;
 Formas forma; 
};


















#endif
