#include "Campo.hpp"
#include "Formas.hpp"
#include <iostream>

using namespace std;

Campo::Campo(Formas f) :forma(f), alternar(true)
{
 for (char i=0; i<altura; i++){
  for (char j=0; j<largura; j++){
    matriz[i][j] = '-';
}
 }
 for (char i=forma.coordenada_y; (i-forma.coordenada_y) < (forma.altura); i++){
  for (char j=forma.coordenada_x; (j - forma.coordenada_x) < (forma.largura); j++){
   if(i<altura && j<largura){
    matriz[i][j] = forma.figura[i-forma.coordenada_y][j-forma.coordenada_x];
    }
  }
 }
}

void Campo::imprime_campo(){
 if (alternar){
  for (char i = 0; i < altura; i++){
   for (char j = 0; j < largura; j++){
    cout << matriz[i][j];
       }
   cout << endl;
   } 
  }
else {
  for (char i = 0; i < altura; i++){
   for (char j = 0; j < largura; j++){
      cout << nova_matriz[i][j];
      }
   cout << endl;
   }
    
  }
  for (char i = 0; i < largura; i++){
   cout << '=';
  }
   cout << endl;
}

void Campo::update(){
 if (alternar){
  for (char i = 0; i < altura; i++){
   for (char j = 0; j < largura; j++){
    nova_matriz[i][j] = Campo::getEstado(matriz[i][j], i, j, alternar);
     }
   }
  alternar = !alternar;
  }
else{
  for (char i=0; i < altura; i++){
   for (char j=0; j < largura; j++){
    matriz[i][j] = Campo::getEstado(nova_matriz[i][j], i, j, alternar);

    }
   }
   alternar = !alternar;
  }
}

char Campo::getEstado(char estado, char coordenada_y, char coordenada_x, bool alternar){
 char vizinhos = 0;
 if (alternar){
  for (char i=coordenada_y - 1; i<=coordenada_y + 1; i++){
   for (char j=coordenada_x - 1; j<=coordenada_x + 1; j++){
    if (i==coordenada_y && j==coordenada_x){
     continue;
}
    if (i > -1 && i < altura && j > -1 && j < largura){
     if (matriz[i][j] == 'X'){
      vizinhos++;
      }
     }
    }
   }
  }
  else{
    for (char i = coordenada_y - 1; i<=coordenada_y + 1; i++){
     for (char j = coordenada_x -1; j<=coordenada_x + 1; j++){
      if (i == coordenada_y && j == coordenada_x){
       continue;
}
   if (i > -1 && i < altura && j > -1 && j < largura){
    if (nova_matriz[i][j] == 'X'){
      vizinhos++;
     }
    }
   }
  }
 }
  if (estado == 'X'){
   return (vizinhos > 1 && vizinhos < 4) ? 'X' : '-';
 }
  else{
   return (vizinhos == 3) ? 'X' : '-';
 }
}

void Campo::iterar(unsigned int iteracoes){
for (int i = 0; i < iteracoes; i++) {
  imprime_campo();
  update();
 }
}






