#include "Formas.hpp"
#include "Glider.hpp"
#include "Blinker.hpp"
#include "Block.hpp"
#include "Campo.hpp"
#include <iostream>
#include <string>

using namespace std;

int main(){
 int x1, y1;
 int x2, y2;
 cout << "########## Bem vindo ao Jogo da Vida ##########"<< endl;
/* cout << "Insira as coordenadas iniciais X e Y da forma Glider: " << endl;
 cin >> x >> y;*/
 cout << "Insira as coordenadas iniciais X e Y da forma Blinker: " << endl;
 cin >> x1 >> y1;
 cout << "Insira as coordenadas iniciais X e Y da forma Block: " << endl;
 cin >> x2 >> y2;
 Glider glider(0,1);
 Campo campo(glider);
 campo.iterar(4);
 Blinker blinker(x1,y1);
 Campo campo2(blinker);
 campo2.iterar(4);
 Block block(x2,y2);
 Campo campo3(block);
 campo3.iterar(4);
}

